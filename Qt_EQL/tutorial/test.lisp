;;; below function calls only work because 'define-qt-wrappers' has already
;;; been called implicitely on '*test*', see 'EQL::addObject()' in 'main.cpp'
;;; (note third argument 'true')
;;;
;;; Qt function names are translated to Lisp names like this:
;;;   Qt:            Lisp:
;;;   newInstance()  (new-instance)
;;;   doIOStuff()    (do-i-o-stuff)

(defun test ()
  ;; make new instance
  (format t "~%Creating instance of 'Test': ~A~%"
          (new-instance *test* *main-widget* "test-1"))
  ;; call Qt slot
  (format t "~%Calling 'Test::concat()': ~S~%"
          (concat *test* (list "one" "two" "three")))
  ;; pass Lisp data
  (format t "~%Processing complex Lisp data in C++:")
  (process-data *test* (list 1 "virus" #\Esc 'the #(l a b)))
  ;; call C++ which will call back to Lisp
  (print-me *test*)
  (terpri))

(defun print-qt-object (object)
  (format t "~%This is an instance of 'Test': ~S~%" object))

(defun repl ()
  ;; for playing around interactively (taken from '~/eql5/src/eql.cpp')
  (setf *qtpl*            t
        *break-on-errors* t)
  (when (directory (in-home "lib/ecl-readline.fas*"))
    (load (x:check-recompile (in-home "lib/ecl-readline"))))
  (qsingle-shot 500 'eql::start-read-thread)
  (eql::exec-with-simple-restart)) ; start event loop

(progn
  (test)
  (qlater 'repl)) ; QLATER: don't block call from 'main.cpp'

