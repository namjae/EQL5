QT          += widgets printsupport uitools
TEMPLATE    = app
CONFIG      += no_keywords release
INCLUDEPATH += /usr/local/include
LIBS        += -L/usr/local/lib -lecl -L$$[QT_INSTALL_LIBS] -leql5 -L. -lmy_app_lib
TARGET      = my_app
DESTDIR     = ./
OBJECTS_DIR = ./tmp/
MOC_DIR     = ./tmp/

win32 {
    include(../src/windows.pri)
}

SOURCES += main.cpp
